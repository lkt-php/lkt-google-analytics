<?php

namespace Lkt\GoogleAnalytics;

class AnalyticsSettings
{
    protected static $USER_ANALYTICS;

    public static function setUserAnalytics(string $ua)
    {
        static::$USER_ANALYTICS = $ua;
    }

    public static function getUserAnalytics() :string
    {
        return trim(static::$USER_ANALYTICS);
    }
}