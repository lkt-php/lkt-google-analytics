<?php

namespace Lkt\GoogleAnalytics\Views;

use Lkt\GoogleAnalytics\AnalyticsSettings;
use Lkt\Templates\Template;

class AnalyticsPageViewViewHandler
{
    public static function getInstance(): string
    {
        $r = new static();
        return $r->parse();
    }

    public function parse(): string
    {
        $userAnalytics = AnalyticsSettings::getUserAnalytics();
        if (!$userAnalytics) {
            return '';
        }

        return Template::file(__DIR__ . '/../../resources/phtml/analytics-page-view.phtml')->setData([
            'userAnalytics' => $userAnalytics,
        ]);
    }
}